// Datei Fibonacci.java

//Paket benennen
package JAHN;

// Klasse eröffnen
public class Fibonacci {

	// main-Methode setzen
	public static void main(String[] args) {

		/*
		 * Erklärung: Fibonacci-Reihe immer Summe der 2 vorhergehenden Zahlen, beginnend
		 * mit 0. also: 0 1 1 2 3 5 8 13 21 34 usw.
		 */
		// erste z Ziffern setzen
		long zahl1 = 0;
		long zahl2 = 1;
		System.out.printf(" 1: %,15d%n", zahl1);
		System.out.printf(" 2: %,15d%n", zahl2);

		// Variable fibo setzen
		long fibo;

		// Begrenzung des Hochzählens auf die ersten 50 Zahlen
		for (int i = 3; i <= 50; i++) {

			// Addieren der Vorgänger und Ausgabe
			fibo = zahl1 + zahl2;
			System.out.printf("%2d: %,15d%n", i, fibo);

			// Zahl 2 wird nach Zahl 1 verschoben
			zahl1 = zahl2;

			// Fibo wird nach Zahl 2 verschoben
			zahl2 = fibo;
			
			// for-Schleife schließen
		}
		
		// Methode schließen
	}
	

	// Klasse schließen
}
