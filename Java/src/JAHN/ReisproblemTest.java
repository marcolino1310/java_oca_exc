// Datei ReisproblemTest.java
// gelaufen am 08.05.2020

// Standardpaket
package JAHN;

// Klasse eröffnen
public class ReisproblemTest {

	// main-Methode
	public static void main(String[] args) {

		/*
		 * Schachbrett mit 64 Feldern. Auf Feld 1 ein Reiskorn. Auf Feld 2 4 Reiskörner.
		 * Auf Feld 3 8 Reiskörner usw. Reiswelternte = 527,4 Mio. Tonnen 1 Reiskorn
		 * wiegt 0,25 Gramm Frage: Wie viele Jahre braucht man, um das Schachbrett zu
		 * füllen?
		 */

		// benötigte Variablen definieren
		double anzahlReiskoerner = 1;
		double gesamtZahl = 0;
		double gesamtGewicht;
		double feld = 1;
		double jahre;

		// Standardausgabe
		System.out.println("Auf Feld 1 kommt 1 Reiskorn.");
		System.out.println();

		// Verdoppelung vornehmen
		for (int i = 1; i <= 64 - 1; i = i + 1) {
			anzahlReiskoerner = anzahlReiskoerner * 2;
			feld = feld + 1;

			// Standardausgabe
			System.out.printf("Auf Feld %.0f kommen %.0f Reiskörner. \n", feld, anzahlReiskoerner);
			System.out.println();
			//

			// Summe alle Felder
			// anzahlReiskoerner += anzahlReiskoerner;
			gesamtZahl += anzahlReiskoerner;

			// Schleife schließen
		}

		// Standardausgabe
		System.out.println("Gesamtzahl ist : " + gesamtZahl + " Reiskörner.");
		System.out.println();

		// Gesamtgewicht der Reiskörner in Tonnen
		gesamtGewicht = gesamtZahl * 0.25 / 1000 / 1000;

		// Standardausgabe
		System.out.printf("Das Gesamtgewicht ist: %.2f Tonnen.%n", gesamtGewicht);
		System.out.println();

		// Gesamtgewicht durch Jahresernte = Anzahl der Jahre
		jahre = gesamtGewicht / 527.4E6;

		// Standardausgabe

		System.out.printf("Gebraucht werden: %.2f Jahre, um das Schachbrett zu füllen.", jahre);
		System.out.println();

		// Methode schließen
	}
	// Klasse schließen
}
