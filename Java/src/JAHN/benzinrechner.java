// Benzinrechner.java
package JAHN;

// Resource Scanner laden
import java.util.Scanner;

// Klasse eröffnen
public class benzinrechner {

	// main-Methode eröffnen
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie die gefahrenen Kilometer ein.");
		double gefahreneKM = sc.nextDouble();
		System.out.println("Bitte geben Sie den Verbrauch in Litern ein.");
		double verbrauchLiter = sc.nextDouble();

		// Berechnung
		double verbrauch100 = verbrauchLiter / gefahreneKM * 100;

		// Standard-Ausgabe
		System.out.println("Sie haben bei " + gefahreneKM + " gefahrenen Kilometern " + verbrauchLiter + " Liter verbraucht.");
		System.out.println("Ihr Verbrauch auf 100 gefahrene Kilometer beträgt: " + verbrauch100 + " Liter.");
		System.out.println();

		// Ausgabe besser formatiert
		System.out.format("Sie haben bei %.1f gefahrenen Kilometern %.1f Liter verbraucht.%n",gefahreneKM, verbrauchLiter);
		System.out.format("Ihr Verbrauch auf 100 gefahrene Kilometer beträgt: %.1f Liter.%n", verbrauch100);
		System.out.println();

		// Resource Scanner schließen!
		sc.close();

		// Methode schließen
	}
	// Klasse schließen
}
