// Datei Rabatt.java

//Paket deklarieren
package JAHN;

// Resource Scanner laden
import java.util.Scanner;

// Klasse eröffnen
public class Rabatt2 {

	// Hauptmethode
	public static void main(String[] args) {

		// In einem Shop werden bei Bestellungen Rabatte gewährt
		// Staffelung: bei Bestellungen ab 100 Euro gibt es 2% Rabatt
		// bei Bestellungen ab 250 Euro gibt es 5% Rabatt
		// bei Bestellungen ab 500 Euro gibt es 8% Rabatt

		// Ausgabe des zu zahlenden Gesamtpreises
		// Testdaten: 75 Euro => 75 Euro
		// 150 Euro => 147 Euro
		// 300 Euro => 285 Euro
		// 1000 Euro => 920 Euro

		// Eingabe der Bestellsumme durch den Benutzer
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie die Bestellsumme ein.");
		double bestellsumme = sc.nextDouble();

		// Variablen setzen
		double rabatt;
		double zu_zahlen;

		/* Bestimmung und Berechnung des Rabatts anhand
		 *  der Bestellsumme. Bei Bestellungen ab 500
		 *   Euro gibt es 8% Rabatt
		 */
		
			if (bestellsumme >= 500) {
			System.out.println("Die Bestellsumme ist: " + bestellsumme + " €.");
			System.out.println("Es gibt 8% Rabatt.");
			rabatt = (bestellsumme * 0.08);
			zu_zahlen = (bestellsumme - rabatt);
			System.out.println("Das sind: " + rabatt + " €.");
			System.out.println("Sie zahlen:" + zu_zahlen + " €.");

			// bei Bestellungen ab 250 Euro gibt es 5% Rabatt
		} else if (bestellsumme >= 250) {
			System.out.println("Die Bestellsumme ist: " + bestellsumme + " €.");
			System.out.println("Es gibt 5% Rabatt.");
			rabatt = (bestellsumme * 0.05);
			zu_zahlen = (bestellsumme - rabatt);
			System.out.println("Das sind: " + rabatt + " €.");
			System.out.println("Sie zahlen:" + zu_zahlen + " €.");

			// bei Bestellungen ab 100 Euro gibt es 2% Rabatt
		} else if (bestellsumme >= 100) {
			System.out.println("Die Bestellsumme ist: " + bestellsumme + " €.");
			System.out.println("Es gibt 2% Rabatt.");
			rabatt = (bestellsumme * 0.02);
			zu_zahlen = (bestellsumme - rabatt);
			System.out.println("Das sind: " + rabatt + " €.");
			System.out.println("Sie zahlen:" + zu_zahlen + " €.");

			// bei Bestellungen unter 100 Euro gibt es keinen Rabatt
		} else if (bestellsumme < 100) {
			System.out.println("Die Bestellsumme ist unter 100 €.");
			System.out.println("Es gibt keinen Rabatt.");
		}

		// Ressource Scanner schließen
		sc.close();

		// 3x else schließen entfällt bei Verwendung " else if"

		// Methode schließen
	}
	// Klasse schließen
}
