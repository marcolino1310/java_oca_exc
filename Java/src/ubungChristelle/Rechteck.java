package ubungChristelle;

public class Rechteck {
	private double laenge;
	private double breite;

	//� Der Konstruktor bekommt die ganzzahlige Breite und H�he des Rechtecks �bergeben.
	public Rechteck(double l, double b) {

		setBreite(l);
		setLaenge(b);
	}
	
	public void setBreite(double breite) {
		this.breite = breite;
	}
	public void setLaenge(double laenge) {
		this.laenge = laenge;
	}
	public double getBreite() {
		return breite;
	}
	public double getLaenge() {
		return laenge;
	}
	
	//� getUmfang() gibt den Umfang des Rechtecks zur�ck. 
	
	public double getUmfang()
	{   
		double umfang= (this.getBreite()+this.getLaenge())*2;
	    return umfang;
	}
	
	//� getFlaeche() gibt die Grundfl�che des Rechtecks zur�ck. 
	
	public double getFlaesche()
	{   
		double grundFlaesche = this.getBreite()*this.getLaenge();
		return grundFlaesche;
	}
	
	//� getDiagonale() gibt die L�nge der Diagonale des Rechtecks aus.
	
	public double getDiagonale()
	{  
	   double diagonal = Math.sqrt( Math.pow(this.getBreite(), 2)+Math.pow(this.getLaenge(), 2));	  
	   return diagonal;
		
	}
	
	//� isQuadrat() �berpr�ft, ob das Rechteck ein Quadrat ist.
	
	public boolean isQuadrat()
	{
	   return this.getBreite()==this.getLaenge();
	  
	}
	
	//Weiterhin soll eine sinnvolle Text-Darstellung des Objekts vorhanden sein, z.B. Rechteck: L�nge x LE, Breite y LE. 
	
	public void objektDarstellung()
	{
		System.out.println("Das Erstellte Rechteck hat folgende Eigenschaften: \n Umfang = "+this.getUmfang()+" m\n GrundFlaesche  = " +this.getFlaesche()
		+ " m� \n Diagonale = "+this.getDiagonale()+ " m \n istQuadrat = "+this.isQuadrat());
	}

}

//Entwickeln Sie eine Klasse namens Rechteck, die ein einfaches zweidimensionales Rechteck abbildet. 
//Nach au�en soll es diese Schnittstellen bereitstellen: 
//	 
//� Der Konstruktor bekommt die ganzzahlige Breite und H�he des Rechtecks �bergeben.
//� getUmfang() gibt den Umfang des Rechtecks zur�ck. 
//� getFlaeche() gibt die Grundfl�che des Rechtecks zur�ck. 
//� getDiagonale() gibt die L�nge der Diagonale des Rechtecks aus. 
//� isQuadrat() �berpr�ft, ob das Rechteck ein Quadrat ist.  
// 
//
//Weiterhin soll eine sinnvolle Text-Darstellung des Objekts vorhanden sein, z.B. Rechteck: L�nge x LE, Breite y LE. 
//Erstellen Sie weiterhin eine Klasse namemn  mamens RechteckTest, das eine Instanz der obigen Klasse erzeugt und testen Sie alle Methoden. 