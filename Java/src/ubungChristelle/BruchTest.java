package ubungChristelle;



public class BruchTest {

	public static void main(String[] args) {
		
		Bruch b1 = new Bruch(); 
		Bruch b2 = new Bruch(4); 
		Bruch b3 = new Bruch(3,4); 
		Bruch b4 = new Bruch(2,3);
		b1.info(); // 1/1
		b2.info(); // 4/1
		b3.info(); // 3/4s
		b4.info(); // 2/3
		System.out.print("b3*b4: ");
		b3.multi(b4).info();
		System.out.print("b3/b4: ");
		b3.divi(b4).info();
		System.out.print("b3+b4: ");
		b3.addi(b4).info();
		System.out.print("Inverse von b3: ");
		b3.inv().info();
		

	}

}
