package ubungChristelle;

public class Bruch {

//	2 Variablen
	private int z1; // Z�hler
	private int z2; //Nenner

//	3 Konstuktoren
	public Bruch() {
		this(1,1);

	}

	public Bruch(int a) {
		this(a, 1);
	}

	public Bruch(int a, int b) {
		setZ1(a);
		setZ2(b);
	}

	public void setZ1(int z1) {
			this.z1 = z1;
		
	}

	public void setZ2(int z2) {
		if (z2 != 0)
			this.z2 = z2;
		else
	     throw new IllegalArgumentException("Der Nenner kann nicht gleich 0 sein");
	}

	public int getZ1() {
		return z1;
	}

	public int getZ2() {
		return z2;
	}

	public Bruch multi(Bruch a) {
		int z = this.z1 * a.z1;
		int n = this.z2 * a.z2;
		Bruch f = new Bruch(z, n);
		return f;

	}

	public Bruch divi(Bruch a) {
		int z = this.z1 * a.z2;
		int n = this.z2 * a.z1;
		Bruch f = new Bruch(z, n);
		return f;

	}

	public Bruch addi(Bruch a) {
		int z = (this.z1 * a.z2) + (this.z2 * a.z1);
		int n = this.z2 * a.z2;
		Bruch f = new Bruch(z, n);
		return f;

	}

	public Bruch inv() {
		Bruch f = new Bruch(this.z2, this.z1);
		return f;
	}

//	info-Methode
	public void info() {
		System.out.println("Z�hler = " + this.getZ1() + " Nenner = " + this.getZ2());
	}
}
