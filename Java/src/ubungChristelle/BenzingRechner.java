package ubungChristelle;

import java.util.Scanner;

public class BenzingRechner {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte gefahrener Kilometer eingeben");
		double km=sc.nextDouble();
		System.out.println("Bitte der Verbrauch in Liter eingeben");
		double liter=sc.nextDouble();
		double z= liter/(km*100);
		
		System.out.println("Sie haben oder werden auf 100 Km " +z+ " Liter verbrauchen");
		
		//System.out.format("Sie haben oder werden auf 100 Km %.1f Liter verbrauchen", z);
		
        sc.close();
	}

}
