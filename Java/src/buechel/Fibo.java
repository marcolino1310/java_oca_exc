package buechel;

public class Fibo {

  public static void main(String[] args) {
    long zahl1 = 0;
    long zahl2 = 1;
    System.out.println(zahl1);
    System.out.println(zahl2);

    long fibo;

    for (int i = 3; i <= 50; i++) {

      fibo = zahl1 + zahl2;
      System.out.printf("%,d%n", fibo);

      zahl1 = zahl2;

      zahl2 = fibo;

    }
  }
}
