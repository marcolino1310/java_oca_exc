package kotrade.uebung1;

import java.text.NumberFormat;
import java.util.Scanner;
import java.lang.Math;

public class Uebung2C {

	public static void main(String[] args) {
		
		// Program um entweder die Kreditlaufzeit oder das Endkapital zu berechnen

		Scanner sc = new Scanner(System.in);

		System.out.println("Möchten Sie die Laufzeit (1) oder das Endkapital (2) berechnen?");
		int funktionsAuswahl = sc.nextInt();

		if (funktionsAuswahl == 1) {
			System.out.println("Sie haben die Berechnung des Endkapitals gewählt.");
//			Programmieren Sie einen Sparzinsrechner, der folgendes leistet: der Anwender kann über 3 Eingaben das Startkapital, 
//			die Laufzeit in Jahren sowie den Zinssatz eingeben. Ausgegeben werden soll das Endkapital am Ende der Laufzeit.

			// Eingabe der drei Werte
			System.out.println("Bitte geben Sie das Startkapital ein.");
			int startkapital = sc.nextInt();
			System.out.println("Bitte geben Sie den Zinssatz in Prozent ein.");
			double zinssatz = sc.nextDouble();
			System.out.println("Bitte geben Sie die Laufzeit in Jahren ein.");
			int laufzeit = sc.nextInt();

			// Verarbeitung: Berechnung des Endkapitals
			double endkapital = ((startkapital * zinssatz * laufzeit) / 100) + startkapital; // Zinsformel
			endkapital = startkapital * Math.pow(1 + zinssatz / 100, laufzeit); // Zinseszinsformel

			// Ausgabe des Endkapital
			// Test 1000 - 5 - 5 => 1276,28
			System.out.println("Ihr Endkapital beträgt: " + endkapital);
			System.out.printf("Ihr Endkapital beträgt: %,.2f €%n", endkapital);
			System.out.println("Ihr Endkapital beträgt: " + NumberFormat.getCurrencyInstance().format(endkapital));

			// Ausgabe der Kapitalentwicklung
			// 1 1050,00
			// 2 1110,56
			// ...
			// 10 1628,65

			for (int jahr = 1; jahr <= laufzeit; jahr++) {
//				System.out.println(jahr + " " + startkapital * Math.pow(1 + zinssatz / 100, jahr));
				System.out.printf("%3d %,.2f €%n", jahr, startkapital * Math.pow(1 + zinssatz / 100, jahr));

			}

		} else if (funktionsAuswahl == 2) {
			System.out.println("Sie haben die Berechnung der Laufzeit gewählt.");

			// Eingabe der drei Werte
			System.out.println("Bitte geben Sie das Startkapital ein.");
			int startkapital = sc.nextInt();
			System.out.println("Bitte geben Sie das Endkapital ein.");
			int endkapital = sc.nextInt();
			System.out.println("Bitte geben Sie den Zinssatz in Prozent ein.");
			double zinssatz = sc.nextDouble();
//			q = 1+p/100
//			laufzeit = log(Kn/K0)/log(q) 
//			laufzeit = log(Endkapital/Startkapital) / log(1+Prozent/100)
//			double laufzeit = Math.log(200000/100000) / Math.log(1 + 5 / 100);	
			double laufzeit = Math.log(endkapital/startkapital) / Math.log(1 + zinssatz / 100);	
			System.out.printf("Die Laufzeit beträgt: %.2f Jahre.", laufzeit); 

		} else {
			System.out.println("Fehlerhafte Eingabe!");
		}

		sc.close();
	}
}
