package plitz.util;

public class Utility
{
	public static double roundToPrecision(double d, int precision)
	{
		return (double) (Math.round(d * Math.pow(10, precision))) / Math.pow(10, precision);
	}

	public static boolean isPrime(int i)
	{
		if (i < 2)
		{
			return false;
		}

		if (i == 2)
		{
			return true;
		}

		for (int j = 2; j <= Math.sqrt(i); j++)
		{
			if (i % j == 0)
			{
				return false;
			}
		}

		return true;
	}

	public static int ggT(int a, int b)
	{
		if (b == 0)
		{
			return a;
		}
		else
		{
			return ggT(b, a % b);
		}
	}

	public static boolean contains(int i, int[] array)
	{
		for (int j = 0; j < array.length; j++)
		{
			if (i == array[j])
			{
				return true;
			}
		}
		return false;
	}
}
