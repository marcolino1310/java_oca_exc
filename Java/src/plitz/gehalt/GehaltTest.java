package plitz.gehalt;

public class GehaltTest
{
	public static void main(String[] args)
	{
		Person[] personen = new Person[50];
		Person oberBoss = new LeitenderAngestellter("Oberboss", 99999);

		for (int i = 0; i < personen.length; i++)
		{
			if (i % 10 == 0)
			{
				personen[i] = new LeitenderAngestellter("Abteilungsleiter", 2000 + (int) (Math.random() * 500));
			}
			else
			{
				personen[i] = new Angestellter("Angestellter", (int) (Math.random() * 50));
			}
		}

		Person.print(oberBoss, personen);
	}
}
