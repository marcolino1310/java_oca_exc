package plitz.gehalt;

public class Angestellter extends Person
{
	private static double ueberstundenVerguetung = 50;

	private int ueberstunden;

	public Angestellter(String name, int ueberstunden)
	{
		super(name, 2000);
		setUeberstunden(ueberstunden);
	}

	public int getUeberstunden()
	{
		return ueberstunden;
	}

	public void setUeberstunden(int ueberstunden)
	{
		if (ueberstunden < 0)
		{
			throw new IllegalArgumentException("Überstunden darf nicht kleiner als 0 sein." + ueberstunden);
		}

		this.ueberstunden = ueberstunden;
	}

	@Override
	public double berechneGehalt()
	{
		return getGehalt() + ueberstunden * ueberstundenVerguetung;
	}
}
