package plitz.gehalt;

public abstract class Person
{
	private String name;
	private double gehalt;

	public Person(String name, double gehalt)
	{
		setName(name);
		setGehalt(gehalt);
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		if (name == null || name.length() < 1)
		{
			throw new IllegalArgumentException("Name darf nicht leer sein oder weniger als 1 Buchstaben habe." + name);
		}

		this.name = name;
	}

	public double getGehalt()
	{
		return gehalt;
	}

	public void setGehalt(double gehalt)
	{
		this.gehalt = gehalt;
	}

	public abstract double berechneGehalt();

	public static void print(Person p, Person... personen)
	{
		double summe = p.berechneGehalt();
		
		System.out.printf("%s%n", p);
		
		for (Person i: personen)
		{
			System.out.printf("%s%n", i);
			summe += i.berechneGehalt();
		}
		
		System.out.printf("Summe: %.2f €%n", summe);
	}

	@Override
	public String toString()
	{
		return String.format("%s: %,.2f €", getName(), berechneGehalt());
	}
}
