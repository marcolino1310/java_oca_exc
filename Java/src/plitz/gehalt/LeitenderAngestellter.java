package plitz.gehalt;

public class LeitenderAngestellter extends Person
{
	private double bonus;

	public LeitenderAngestellter(String name, double bonus)
	{
		super(name, 3000);
		this.setBonus(bonus);
	}

	public double getBonus()
	{
		return bonus;
	}

	public void setBonus(double bonus)
	{
		if (bonus < 0)
		{
			throw new IllegalArgumentException("Bonus darf nicht kleiner als 0 sein." + bonus);
		}

		this.bonus = bonus;
	}

	@Override
	public double berechneGehalt()
	{
		return getGehalt() + bonus;
	}

}
