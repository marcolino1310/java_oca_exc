package plitz.aufagbe2;

import java.util.Scanner;

public class DreieckTest
{

	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		double a = 0;
		double b = 0;
		double c = 0;

		while (true)
		{
			try
			{
				System.out.println("Geben Sie die Länge der ersten Seite ein:");
				a = scanner.nextDouble();

				System.out.println("Geben Sie die Länge der zweiten Seite ein:");
				b = scanner.nextDouble();

				System.out.println("Geben Sie die Länge der dritten Seite ein:");
				c = scanner.nextDouble();
			}
			catch (Exception e)
			{
				System.out.println("Fehlerhafte Eingabe.");
				continue;
			}

			if (a <= 0 || b <= 0 || c <= 0)
				break;

			Dreieck d;
			
			try
			{
				d = new Dreieck(a, b, c);
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				continue;
			}
			
			d.info();
			System.out.println();
		}

		System.out.printf("Es wurden %d Dreiecke erstellt", Dreieck.getCount());
		scanner.close();
	}

}
