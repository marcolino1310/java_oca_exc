package plitz.aufagbe2;

import java.time.*;

public class Mensch
{
	private String name;
	private String geburtsDatum;

	public Mensch(String name, String geburtsDatum)
	{
		this.name = name;
		this.geburtsDatum = geburtsDatum;
	}

	String getName()
	{
		return name;
	}

	void setName(String name)
	{
		this.name = name;
	}

	String getGeburtsDatum()
	{
		return geburtsDatum;
	}

	void setGeburtsDatum(String geburtsDatum)
	{
		this.geburtsDatum = geburtsDatum;
	}

	int getAlter()
	{
		LocalDate now = LocalDate.now();

		int year = Integer.parseInt(geburtsDatum.subSequence(6, 10).toString());
		int month = Integer.parseInt(geburtsDatum.subSequence(3, 5).toString());
		int dayOfMonth = Integer.parseInt(geburtsDatum.subSequence(0, 2).toString());

		LocalDate gebDate = LocalDate.of(year, month, dayOfMonth);

		Period period = Period.between(gebDate, now);
		int diff = period.getYears();

		return diff;
	}

	void getInfo()
	{
		System.out.printf("%s: %d%n", name, getAlter());
	}
}
