package plitz.aufagbe2;

import java.util.Scanner;

public class RechteckTest
{

	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		double height = 0;
		double width = 0;

		while (true)
		{
			try
			{
				System.out.println("Geben Sie die Höhe ein:");
				height = scanner.nextDouble();

				System.out.println("Geben Sie die Breite ein:");
				width = scanner.nextDouble();
			}
			catch (Exception e)
			{
				System.out.println("Fehlerhafte Eingabe.");
				continue;
			}

			if (height <= 0 || width <= 0)
				break;

			Rechteck r = new Rechteck(height, width);
			r.print();
			System.out.println();
			System.out.printf("Fläche: %.2f%nUmfang: %.2f%nDiagonale: %.2f%n", r.getArea(), r.getCircumference(),
					r.getDiagonal());
			System.out.printf("Ist ein Quadrat: %s%n%n", (r.isSquare() ? "Ja" : "Nein"));
		}

		System.out.printf("Es wurden %d Rechtecke erstellt", Rechteck.getCount());
		scanner.close();
	}

}
