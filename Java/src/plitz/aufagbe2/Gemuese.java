package plitz.aufagbe2;

public class Gemuese
{

	public static void main(String[] args)
	{
		double[][] Kasse = new double[52][6];

		for (int i = 0; i < Kasse.length; i++)
		{
			for (int j = 0; j < Kasse[i].length; j++)
			{
				Kasse[i][j] = Math.random() * 470 + 650 + (j == 5 ? 0 : 200);
				Kasse[i][j] = plitz.util.Utility.roundToPrecision(Kasse[i][j], 2);
			}
		}

		for (int i = 0; i < Kasse.length; i++)
		{
			double temp = 0;
			for (int j = 0; j < Kasse[i].length; j++)
			{
				// System.out.printf("%,8.2f € ",Kasse[i][j]);
				temp += Kasse[i][j];
			}
			System.out.printf("%2d: %,8.2f € |", i + 1, temp);
			if (i % 5 == 4)
			{
				System.out.println();
			}
		}
	}

}
