package plitz.aufagbe2;

import java.util.Scanner;

public class Rabatt
{
	public static void main(String[] args)
	{
		/*
		 * 
		 * ab 100 2% ab 250 5% ab 500 8%
		 * 
		 */

		Scanner scanner = new Scanner(System.in);
		double summe;
		double rebate;

		do
		{
			try
			{
				System.out.println("Bitte geben Sie die Bestellsumme ein:");
				summe = scanner.nextDouble();
			}
			catch (Exception e)
			{
				System.out.println("Falsche Eingabe");
				summe = 0;
				continue;
			}

			if (summe < 100)
			{
				rebate = 0;
			}
			else
			{
				if (summe < 250)
				{
					rebate = 0.02;
				}
				else
				{
					if (summe < 500)
					{
						rebate = 0.05;
					}
					else
					{
						rebate = 0.08;
					}
				}
			}

			if (summe > 0)
			{
				System.out.printf("Der Gesamtpreis beträgt %,.2f €%n%n", summe * (1 - rebate));
			}
		} while (summe > 0);

		scanner.close();
	}
}
