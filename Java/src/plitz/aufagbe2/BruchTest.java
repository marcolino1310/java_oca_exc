package plitz.aufagbe2;

import java.util.Scanner;

public class BruchTest
{
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		int z;
		int n;

		while (true)
		{
			System.out.println("Bruch (0/0 zum beenden):");
			try
			{
				System.out.println("Bitte geben Sie den Zähler ein:");
				z = scanner.nextInt();

				System.out.println("Bitte geben Sie den Nenner ein:");
				n = scanner.nextInt();
			}
			catch (Exception e)
			{
				System.out.println("Fehlerhafte Eingabe! " + scanner.next()); // skip wrong input
				continue;
			}

			if (n == 0 && z == 0)
			{
				break;
			}

			Bruch b;

			try
			{
				b = new Bruch(z, n);
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				continue;
			}

			b.info();
			System.out.println("Mal 1/4");
			b.multi(new Bruch(1, 4)).info();
			System.out.println("Durch 1/4");
			b.divide(new Bruch(-2, 3)).info();
			System.out.println("Plus 1/4");
			b.add(new Bruch(1, 4)).info();
			System.out.println("Minus 1/4");
			b.sub(new Bruch(1, 4)).info();
			System.out.println();
		}

		Bruch dez = new Bruch(0.4);
		dez.info();

		scanner.close();
	}
}
