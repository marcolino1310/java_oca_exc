package plitz.aufagbe2;

import java.util.Scanner;

public class Factorial
{

	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		int input = 0;
		
		while(true)
		{
			try
			{
				input = scanner.nextInt();
			}
			catch (Exception e) 
			{
				input = -1;
			}
			
			if(input == 0)
			{
				break;
			}
			
			System.out.printf("%,d%n", factor(input));
		}
		
		scanner.close();
	}

	public static long factor(long i)
	{
		return i > 1L ? factor(i - 1L) * i : i;
	}
}
