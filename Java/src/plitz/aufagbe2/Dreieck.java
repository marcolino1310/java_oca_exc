package plitz.aufagbe2;

public class Dreieck
{
	private static int count = 0;

	private double a;
	private double b;
	private double c;

	public Dreieck()
	{
		this(1, 1, 1);
	}

	public Dreieck(double a)
	{
		this(a, a, a);
	}

	public Dreieck(double a, double b)
	{
		this(a, b, a);
	}

	public Dreieck(double a, double b, double c)
	{
		checkSeiten(a, b, c);

		double swap;

		if (a > b)
		{
			swap = b;
			b = a;
			a = swap;
		}

		if (b > c)
		{
			swap = c;
			c = b;
			b = swap;
		}

		if (a > b)
		{
			swap = b;
			b = a;
			a = swap;
		}

		this.setA(a);
		this.setB(b);
		this.setC(c);
		count++;
	}

	private static void checkSeite(double s)
	{
		if (s <= 0)
		{
			throw new IllegalArgumentException("Sides must be positive.");
		}
	}

	private static void checkSeiten(double a, double b, double c)
	{
		if (a + b <= c || a + c <= b || b + c <= a)
		{
			throw new IllegalArgumentException("Illegal Triangle: " + a + " " + b + " " + c);
		}
	}

	public static int getCount()
	{
		return count;
	}

	public double getA()
	{
		return a;
	}

	public void setA(double a)
	{
		checkSeite(a);
		this.a = a;
	}

	public double getB()
	{
		return b;
	}

	public void setB(double b)
	{
		checkSeite(a);
		this.b = b;
	}

	public double getC()
	{
		return c;
	}

	public void setC(double c)
	{
		checkSeite(a);
		this.c = c;
	}

	public void info()
	{
		System.out.printf("Dreieck mit %.2f, %.2f, %.2f%n", a, b, c);
		System.out.printf("Hat eine Fläche von %.2f%n", getArea());
		System.out.printf("Hat einen Umfang von %.2f%n", getCircumference());
		System.out.printf("Alpha ist %.2f%n", getAlpha());
		System.out.printf("Beta ist %.2f%n", getBeta());
		System.out.printf("Gamma ist %.2f%n", getGamma());
		System.out.printf("Ist %srechtwinklig.%n", (isRectangular() ? "" : "nicht "));
		System.out.printf("Ist %sgleichseitig.%n", (isEquilateral() ? "" : "nicht "));
	}

	public double getArea()
	{
		double s = getCircumference() / 2;

		return Math.sqrt(s * (s - a) * (s - b) * (s - c));
	}

	public double getCircumference()
	{
		return a + b + c;
	}

	public boolean isEquilateral()
	{
		return a == b && b == c;
	}

	public boolean isRectangular()
	{
		/*
		 * double alpha = Math.acos((a * a - b * b - c * c) / (-2 * b * c)); double beta
		 * = Math.acos((b * b - a * a - c * c) / (-2 * a * c)); double gamma =
		 * Math.acos((c * c - b * b - a * a) / (-2 * b * a));
		 */

		return a * a + b * b == c * c;

		// return alpha == Math.PI / 2 || beta == Math.PI / 2 || gamma == Math.PI / 2;
	}

	public double getAlpha()
	{
		return Math.acos((a * a - b * b - c * c) / (-2 * b * c)) * 180 / Math.PI;
	}

	public double getBeta()
	{
		return Math.acos((b * b - a * a - c * c) / (-2 * a * c)) * 180 / Math.PI;
	}

	public double getGamma()
	{
		return Math.acos((c * c - b * b - a * a) / (-2 * b * a)) * 180 / Math.PI;
	}
}
