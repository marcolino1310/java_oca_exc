package plitz.aufagbe2;

import java.util.Scanner;

public class LottoTest
{

	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);

		int[] lotto = new int[6];

		for (int i = 0; i < lotto.length; i++)
		{
			boolean repeat = true;
			int temp = 0;

			while (repeat)
			{
				repeat = false;
				temp = (int) (Math.random() * 49) + 1;

				for (int j = 0; j < i; j++)
				{
					if (lotto[j] == temp)
						repeat = true;
				}
			}
			
			lotto[i] = temp;
		}

		for (int i : lotto)
		{
			System.out.println(i);
		}

		scanner.close();
	}

}
