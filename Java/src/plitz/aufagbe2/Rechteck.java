package plitz.aufagbe2;

public class Rechteck
{
	private double width;
	private double height;
	static private int count = 0;

	public Rechteck(double width)
	{
		this(width, width);
	}

	public Rechteck(double height, double width)
	{
		setHeight(height);
		setWidth(width);
		count++;
	}

	public Rechteck()
	{
		this(1, 1);
	}

	public double getWidth()
	{
		return width;
	}

	public void setWidth(double width)
	{
		this.width = Math.abs(width);
	}

	public double getHeight()
	{
		return height;
	}

	public void setHeight(double height)
	{
		this.height = Math.abs(height);
	}

	public double getArea()
	{
		return width * height;
	}

	public double getCircumference()
	{
		return 2 * (width + height);
	}

	public double getDiagonal()
	{
		return Math.sqrt(height * height + width * width);
	}

	public boolean isSquare()
	{
		return width == height;
	}

	public void print()
	{
		if (isSquare())
		{
			System.out.printf("Seitenlänge: %.2f", height);
		}
		else
		{
			System.out.printf("Höhe: %.2f, Breite: %.2f", height, width);
		}
	}

	public boolean equals(Rechteck r)
	{
		if (r == this)
		{
			return true;
		}

		return this.height == r.getHeight() && this.width == r.getWidth();
	}
	
	public static int getCount()
	{
		return count;
	}
}
