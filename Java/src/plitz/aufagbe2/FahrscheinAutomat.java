package plitz.aufagbe2;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class FahrscheinAutomat
{
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		String input = "";
		Tarif tarif;

		outer:
		do
		{
			System.out.println("Bitte wählen Sie die Tarifzone aus: K, A, B, C, D");

			input = scanner.nextLine();

			if (!input.equals("")) // do nothing if String is empty
			{
				if (input.length() > 0 && Tarif.getTarif(input.toUpperCase().charAt(0)) == Tarif.none) // wrong input ->
																										// try again
				{
					System.out.println("Falsche Auswahl. Bitte versuchen Sie es erneut.");
					continue;
				}
				else
				{
					tarif = Tarif.getTarif(input.toUpperCase().charAt(0));
				}

				System.out.printf("Der Preis beträgt: %.2f €%n", tarif.getPreis());

				double preis = tarif.getPreis();
				double einwurf;
				String bezahlen;

				do
				{
					try
					{
						System.out.println("Bitte werfen Sie Geld ein (0.1 € bis 20 €, x für Abbruch):");
						bezahlen = scanner.nextLine();

						if (bezahlen.length() > 0 && bezahlen.charAt(0) == 'x')
						{
							continue outer;
						}

						einwurf = Double.parseDouble(bezahlen);
					}
					catch (Exception e)
					{
						einwurf = 0;
						continue;
					}

					if (Geld.isValidEinwurf(einwurf))
					{
						preis -= einwurf;
						preis = plitz.util.Utility.roundToPrecision(preis, 2);
						if (preis > 0)
						{
							System.out.printf("Der Rest beträgt: %.2f €%n", preis);
						}
					}
				} while (preis > 0);

				System.out.println("Das Wechselgeld beträgt:");

				wechselGeld(preis).forEach((k, v) ->
				{
					System.out.printf("%4.2f € x%d%n", k.getWert(), v);
				});
			}
		} while (!input.equals(""));

		scanner.close();
	}

	static Map<Geld, Integer> wechselGeld(double rest)
	{
		Map<Geld, Integer> wechsel = new TreeMap<Geld, Integer>();
		Geld abzug = Geld._2Euro;

		rest = Math.abs(rest);

		for (Geld i : Geld.values())
		{
			wechsel.put(i, 0);
		}

		while (rest > 0)
		{
			if (rest >= abzug.getWert())
			{
				rest -= abzug.getWert();
				wechsel.replace(abzug, wechsel.get(abzug) + 1);
			}
			else
			{
				abzug = abzug.getNext();
			}
			rest = plitz.util.Utility.roundToPrecision(rest, 2);
		}

		wechsel.entrySet().removeIf(e -> e.getValue() == 0);

		return wechsel;
	}
}

enum Tarif
{
	none(-1), K(1.4), A(2.3), B(4.7), C(9.6), D(11.4);

	private double preis;
	
	Tarif(double preis)
	{
		this.preis = preis;
	}

	double getPreis()
	{
		return preis;
	}

	static Tarif getTarif(char c)
	{
		switch (c)
		{
		case 'K':
			return K;
		case 'A':
			return A;
		case 'B':
			return B;
		case 'C':
			return C;
		case 'D':
			return D;
		default:
			return none;
		}
	}
};

enum Geld
{
	none(.0), _1Cent(.01), _2Cent(.02), _5Cent(.05), _10Cent(.1), _20Cent(.2), _50Cent(.5), _1Euro(1), _2Euro(2),
	_5Euro(5), _10Euro(10), _20Euro(20);

	private double wert;

	Geld(double wert)
	{
		this.wert = wert;
	}

	static boolean isValidEinwurf(double einwurf)
	{
		switch ((int) (plitz.util.Utility.roundToPrecision(einwurf, 2) * 100))
		{
		case 10:
		case 20:
		case 50:
		case 100:
		case 200:
		case 500:
		case 1000:
		case 2000:
			return true;

		default:
			return false;
		}

	}

	double getWert()
	{
		return wert;
	}

	Geld getNext()
	{
		switch (this)
		{
		case _20Euro:
			return _10Euro;
		case _10Euro:
			return _5Euro;
		case _5Euro:
			return _2Euro;
		case _2Euro:
			return _1Euro;
		case _1Euro:
			return _50Cent;
		case _50Cent:
			return _20Cent;
		case _20Cent:
			return _10Cent;
		case _10Cent:
			return _5Cent;
		case _5Cent:
			return _2Cent;
		case _2Cent:
			return _1Cent;
		default:
			return none;
		}
	}
}