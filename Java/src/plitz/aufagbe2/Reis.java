package plitz.aufagbe2;

import java.math.BigInteger;

public class Reis
{

	public static void main(String[] args)
	{
		BigInteger reis = BigInteger.ONE;// Das erste Feld
		BigInteger reisSumme = BigInteger.ONE;// Die erste Summe
		System.out.printf("Auf dem Feld %d ist %,d Reiskorn.%n", 1, reis);
		
		for (int i = 1; i < 64; i++)
		{
			reis = reis.multiply(BigInteger.valueOf(2));
			reisSumme = reisSumme.add(reis);
			System.out.printf("Auf dem Feld %d sind %,d Reiskörner.%n", i + 1, reis);
		}
		System.out.printf("Es sind %,15d Resikörner%n", reisSumme);
		System.out.printf("Sie wiegen %,.2f Tonnen%n", reisSumme.doubleValue() * 0.25 / 1000000);
		System.out.printf("Es werden %,.2f Jahre benötigt.", reisSumme.doubleValue()* 0.25 / 1000000 / 527_400_000.);
		
		/*
		 * System.out.printf("Es sind %,15d Resikörner%n",
		 * BigInteger.valueOf(2).pow(64).subtract(BigInteger.ONE));
		 */
	}

}
