package plitz.aufagbe2;

public class Bruch
{
	private int z;
	private int n;

	public Bruch()
	{
		this(1, 1);
	}

	public Bruch(int z)
	{
		this(z, 1);
	}

	public Bruch(int z, int n)
	{
		setZ(z);
		setN(n);
		kuerzen();
	}

	public Bruch(double dez)
	{
		String s_dez = String.valueOf(dez);

		int digitsDec = s_dez.length() - 1 - s_dez.indexOf('.');

		int denom = 1;
		for (int i = 0; i < digitsDec; i++)
		{
			dez *= 10;
			denom *= 10;
		}
		int num = (int) (dez);

		setZ(num);
		setN(denom);
		kuerzen();
	}

	public int getZ()
	{
		return z;
	}

	private void setZ(int z)
	{
		this.z = z;
	}

	public int getN()
	{
		return n;
	}

	private void setN(int n)
	{
		if (n == 0)
		{
			throw new IllegalArgumentException("Nenner darf nicht Null sein");
		}

		if (n < 0)
		{
			setZ(-n);
			n = -n;
		}

		this.n = n;
	}

	public void info()
	{
		if (Math.abs(z) > n)
		{
			System.out.printf("%d %d/%d (%4f)%n", z / n, Math.abs(z % n), n, getDecimal());
		}
		else
		{
			if (z % n == 0)
			{
				System.out.printf("%d", z / n);
			}
			else
			{
				System.out.printf("%d/%d (%4f)%n", z, n, getDecimal());
			}
		}
	}

	public Bruch multi(Bruch b)
	{
		Bruch r = new Bruch(z * b.getZ(), n * b.getN());
		return r;
	}

	public Bruch divide(Bruch b)
	{
		Bruch r = new Bruch(z * b.getN(), n * b.getZ());
		return r;
	}

	public Bruch add(Bruch b)
	{
		Bruch r = new Bruch(z * b.getN() + b.getZ() * n, n * b.getN());
		return r;
	}

	public Bruch sub(Bruch b)
	{
		Bruch r = new Bruch(z * b.getN() - b.getZ() * n, n * b.getN());
		return r;
	}

	public double getDecimal()
	{
		return 1. * z / n;
	}

	public void kuerzen()
	{
		int i = plitz.util.Utility.ggT(z, n);
		while (i > 1)
		{
			setN(n / i);
			setZ(z / i);
			i = plitz.util.Utility.ggT(z, n);
		}
	}

	public Bruch getTnvert()
	{
		Bruch b = new Bruch(n, z);
		return b;
	}
}
