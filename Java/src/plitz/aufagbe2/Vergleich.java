package plitz.aufagbe2;

public class Vergleich
{

	public static void main(String[] args)
	{
		int[] array1 = new int[10];
		int[] array2 = new int[10];
		int count = 0;
		final int AMOUNT = 10_000_000;
		final int STEP = 10;
		final int PERCENT = AMOUNT / 100 * STEP;

		for (int j = 0; j < AMOUNT; j++)
		{
			for (int i = 0; i < array1.length; i++)
			{
				array1[i] = (int) (Math.random() * 51);
			}

			for (int i = 0; i < array2.length; i++)
			{
				array2[i] = (int) (Math.random() * 51);
			}

			/*
			 * for (int i = 0; i < array1.length; i++) { System.out.printf("%2d - %2d%n",
			 * array1[i], array2[i]); }
			 */

			if ((j + 1) % PERCENT == 0)
			{
				System.out.printf("%3.0f%%%n", 1. * STEP * j / PERCENT);
			}

			for (int i = 0; i < array1.length; i++)
			{

				if (plitz.util.Utility.contains(array1[i], array2))
				{
					count++;
					break;
				}

			}
		}

		System.out.printf("%,3d Versuche - %,3d Übereinstimmungen - %.2f %%", AMOUNT, count, 100. * count / AMOUNT);
	}

}
