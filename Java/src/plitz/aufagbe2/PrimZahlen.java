package plitz.aufagbe2;

public class PrimZahlen
{

	public static void main(String[] args)
	{
		int count = 1;
		System.out.println("Primzahlen");
		
		if(plitz.util.Utility.isPrime(2))
		{
			System.out.printf("%4d%n", 2);
		}
		
		for (int i = 3; i <= 1000; i+=2)
		{
			if(plitz.util.Utility.isPrime(i))
			{
				System.out.printf("%4d%n", i);
				count++;
			}
		}
		System.out.printf("Es sind %d Primzahlen.%n", count);
	}

}
