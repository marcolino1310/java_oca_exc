package plitz.aufgabe1;

public class HundertTeiler
{

	public static void main(String[] args)
	{
		long time = System.currentTimeMillis();
		for (int i = 1; i <= 50000; i++)
		{
			System.out.printf("Die Teiler von %d sind:%n", i);
			for (int j : GGT.listeVonTeilern(i))
			{
				System.out.printf("%d ", j);
			}
			System.out.println();
		}
		System.out.printf("%d ms", System.currentTimeMillis() - time);
	}

}
