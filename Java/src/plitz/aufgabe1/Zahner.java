package plitz.aufgabe1;

public class Zahner
{
	public static void main(String[] args)
	{
		for (int i = 1; i <= 100; i += 10)
		{
			for (int j = 0; j < 10; j++)
			{
				System.out.printf("%3d ", j + i);
			}
			System.out.println();
		}
	}
}
