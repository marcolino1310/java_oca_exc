package plitz.aufgabe1;

public class Fibo
{

	public static void main(String[] args)
	{
		/*
		 * long number1 = 0; long number2 = 1; long number3;
		 * 
		 * System.out.printf("%2d: %,14d%n", 1, number1);
		 * System.out.printf("%2d: %,14d%n", 2, number2); for (int i = 2; i < 50; i++) {
		 * number3 = number1 + number2; System.out.printf("%2d: %,14d%n", i + 1,
		 * number3); number1 = number2; number2 = number3; }
		 */

		final int ANZAHL = 50;

		long[] fibo = new long[ANZAHL];
		fibo[0] = 0;
		fibo[1] = 1;

		for (int i = 2; i < fibo.length; i++)
		{
			fibo[i] = fibo[i - 1] + fibo[i - 2];
		}

		for (int i = 0; i < fibo.length; i++)
		{
			System.out.printf("%2d: %,14d%n", i + 1, fibo[i]);
		}
	}

}
