package plitz.aufgabe1;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Datum
{
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		String datum;

		do
		{
			datum = scanner.nextLine();
			if(isValid(datum))
			{
				System.out.println("Datum ist correct.");
			}
			else
			{
				System.out.println("Datum ist nicht correct.");
			}
		} while (!datum.equals(""));

		scanner.close();
	}

	public static boolean isValid(String datum)
	{
		List<String> list = Arrays.asList(datum.split("[.]"));
		int day = 0;
		int month = 0;
		int year = 0;

		if (list.size() != 3)
			return false;
		if (list.get(0).length() != 2 || list.get(1).length() != 2 || list.get(2).length() != 4)
			return false;

		try
		{
			day = Integer.parseInt(list.get(0));
			month = Integer.parseInt(list.get(1));
			year = Integer.parseInt(list.get(2));
		}
		catch (Exception e)
		{
			return false;
		}

		if (day < 1 || month < 1 || year < 1)
			return false;

		switch (month)
		{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return day <= 31;
		case 4:
		case 6:
		case 9:
		case 11:
			return day <= 30;
		case 2:
			return day <= (LeapYear.isLeapYear(year) ? 29 : 28);
		default:
			return false;
		}
	}
}
