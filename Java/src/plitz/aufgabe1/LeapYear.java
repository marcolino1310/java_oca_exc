package plitz.aufgabe1;

import java.util.Scanner;

public class LeapYear
{
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		int year;

		do
		{
			System.out.println("Bitte geben Sie ein Jahr ein: ");
			
			try
			{
				year = scanner.nextInt();
			}
			catch (Exception e)
			{
				year = -1;
			}
			
			if (year > 0)
			{
				System.out.println(year + " ist " + (isLeapYear(year) ? "" : "k") + "ein Schaltjahr.");
			}
		} while (year > 0);

		scanner.close();
	}

	public static boolean isLeapYear(int year)
	{
		return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
	}
}
