package plitz.aufgabe1;

import java.util.List;
import java.util.Scanner;
import java.util.Vector;

public class GGT
{
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		int number1 = -1;
		int number2 = -1;

		do
		{
			System.out.println("Bitte geben Sie die erste Zahl ein: ");

			try
			{
				number1 = scanner.nextInt();
			}
			catch (Exception e)
			{
				number1 = -1;
				System.out.println(e.getMessage());
			}

			System.out.println("Bitte geben Sie die zweite Zahl ein: ");

			try
			{
				number2 = scanner.nextInt();
			}
			catch (Exception e)
			{
				number2 = -1;
				System.out.println(e.getMessage());
			}

			System.out.println("Der größte gemeinsame Teiler ist: " + ggT(number1, number2));
			System.out.println("Die Teiler von " + number1 + " sind:");
			for(int i: listeVonTeilern(number1))
			{
				System.out.print(i + " ");
			}

			System.out.println("Die Teiler von " + number2 + " sind:");
			for(int i: listeVonTeilern(number2))
			{
				System.out.print(i + " ");
			}
		} while (number1 > 0 && number2 > 0);

		scanner.close();
	}

	public static int ggT(int a, int b)
	{
		if (b == 0)
		{
			return a;
		}
		else
		{
			return ggT(b, a % b);
		}
	}

	public static List<Integer> listeVonTeilern(int number)
	{
		List<Integer> teiler = new Vector<Integer>();

		for (int i = 1; i <= Math.sqrt(number); i++)
		{
			if (number % i == 0)
			{
				teiler.add(i);
				if (i * i != number)
				{
					teiler.add(number / i);
				}
			}
		}
		teiler.sort(null);
		return teiler;
	}
}