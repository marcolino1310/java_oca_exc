package plitz.aufgabe1;

import java.util.List;

public class VollkommeneZahl
{

	public static void main(String[] args)
	{
//		Eine vollkommene Zahl ist eine natürliche Zahl, deren Teiler, wenn du sie addierst, 
//		die Zahl selbst ergeben. Da jede Zahl durch sich selbst teilbar ist, darfst du den Teiler, 
//		der die Zahl selbst darstellt, nicht hinzuaddieren.
//		Beispiel: 6 = 1 + 2 + 3
//		Ermitteln Sie weitere vollkommene Zahlen.

		long time = System.currentTimeMillis();
		for (int i = 2; i < Integer.MAX_VALUE; i++)
		{
			List<Integer> teiler = plitz.aufgabe1.GGT.listeVonTeilern(i);
			int summe = 0;
			for (int j = 0; j < teiler.size() - 1; j++) // Der letzte Teiler ist die Zahl -> ignorieren
			{
				summe += teiler.get(j);
			}

			if (summe == i)
			{
				System.out.printf("Die Zahl %d ist vollkommen.%n", i);
				System.out.printf("%d - Time %,.2f s%n", i, (System.currentTimeMillis() - time) / 1000.);
			}
			if (Math.log10(i) % 1 == 0)
			{
				System.out.printf("%d - Time %,.2f s%n", i, (System.currentTimeMillis() - time) / 1000.);
			}
		}
	}

}
