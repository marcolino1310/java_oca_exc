package plitz.aufgabe3;

public class ShapeTest
{
	public static void main(String[] args)
	{
		Shape[] shapes = new Shape[50];

		for (int i = 0; i < shapes.length; i++)
		{
			if (i % 2 == 1)
			{
				shapes[i] = new Circle(Math.random() * 25 + 1);
			}
			else
			{
				shapes[i] = new Rectangle(Math.random() * 25 + 1, Math.random() * 25 + 1);
			}
		}
		
		for (Shape s : shapes)
		{
			System.out.println(s);
		}

		System.out.printf("Total Area: %,.2f", Shape.getTotalArea(shapes));
	}
}
