package plitz.aufgabe3;

public class FahrzeugTest
{
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		// Hersteller, Modell, PS, Geschwindigkeit, Anzahl Türen
		Fahrzeug fahrzeug1 = new Auto("Ford", "Focus", 200, 210, 5);
		// Hersteller, Modell, PS, Geschwindigkeit, verkleidet (ja/nein)
		Fahrzeug fahrzeug2 = new Motorrad("Suzuki", "Bandit 600", 90, 220, false);
		// Hersteller, Modell, PS, Zuladung
		Fahrzeug fahrzeug3 = new LKW("Mercedes", "Atego", 250, 7.5);

		// Ausgabe aller Variablen
		System.out.println(fahrzeug1);
		System.out.println(fahrzeug2);
		System.out.println(fahrzeug3);
	}
}
