package plitz.aufgabe3;

public class Train extends TransportVehicle
{
	private String ticketInspector;
	private int numberOfCarriages;
	private int seatPerCarriage;

	public Train(int lineNumber, String driver, String start, String end, String ticketInspector, int numberOfCarriages,
			int seatPerCarriage)
	{
		super(lineNumber, driver, start, end);
		setTicketInspector(ticketInspector);
		setNumberOfCarriages(numberOfCarriages);
		setSeatPerCarriage(seatPerCarriage);
	}

	public String getTicketInspector()
	{
		return ticketInspector;
	}

	public void setTicketInspector(String ticketInspector)
	{
		if (ticketInspector == null || ticketInspector.length() < 1)
		{
			throw new IllegalArgumentException(
					"Der Schaffner darf nicht leer sein oder weniger als einen Buchstaben haben. " + ticketInspector);
		}
		
		this.ticketInspector = ticketInspector;
	}

	public int getNumberOfCarriages()
	{
		return numberOfCarriages;
	}

	public void setNumberOfCarriages(int numberOfCarriages)
	{
		if (numberOfCarriages < 1)
		{
			throw new IllegalArgumentException(
					"Es muss mindestens ein Wagen vorhanden sein. " + numberOfCarriages);
		}
		
		this.numberOfCarriages = numberOfCarriages;
	}

	public int getSeatPerCarriage()
	{
		return seatPerCarriage;
	}

	public void setSeatPerCarriage(int seatsPerCarriage)
	{
		if (seatsPerCarriage < 1)
		{
			throw new IllegalArgumentException(
					"Es muss mindestens ein Sitz pro Wagen vorhanden sein. " + seatsPerCarriage);
		}
		
		this.seatPerCarriage = seatsPerCarriage;
	}

	@Override
	public String toString()
	{
		return super.toString() + String.format(" Der Zug hat %d Wagen und %d Sitze pro Wagen. Der Schaffner ist %s.",
				numberOfCarriages, seatPerCarriage, ticketInspector);
	}
}
