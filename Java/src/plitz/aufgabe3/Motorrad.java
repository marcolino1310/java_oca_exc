package plitz.aufgabe3;

public class Motorrad extends PersonenFahrzeug
{
	private boolean verkleidet;

	public Motorrad()
	{
	}

	public Motorrad(String hersteller, String modell, int ps, double speed, boolean verkleidet)
	{
		super(hersteller, modell, ps, speed);
		setVerkleidet(verkleidet);
	}

	public boolean isVerkleidet()
	{
		return verkleidet;
	}

	public void setVerkleidet(boolean verkleidet)
	{
		this.verkleidet = verkleidet;
	}

	public String toString()
	{
		return super.toString() + " Verkleidet: " + (verkleidet ? "ja" : "nein") + ";";
	}
}
