package plitz.aufgabe3;

public class PublicTransportTest
{
	public static void main(String[] args)
	{
		TransportVehicle train = new Train(5, "Dave", "Hier", "Da", "Kevin", 5, 100);
		
		System.out.println(train);
		
		TransportVehicle bus = new Bus(5, "Eleanor", "Da", "Hier", 25);
		
		System.out.println(bus);
		
		System.out.println(train.equals(bus));
	}
}
