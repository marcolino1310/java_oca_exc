package plitz.aufgabe3;

public abstract class TransportVehicle
{
	private int lineNumber;
	private String driver;
	private String start;
	private String end;

	public TransportVehicle(int lineNumber, String driver, String start, String end)
	{
		setLineNumber(lineNumber);
		setDriver(driver);
		setStart(start);
		setEnd(end);
	}

	public int getLineNumber()
	{
		return lineNumber;
	}

	public void setLineNumber(int lineNumber)
	{
		if (lineNumber < 1)
		{
			throw new IllegalArgumentException("Liniennummer darf nicht kleiner als 1 sein. " + lineNumber);
		}

		this.lineNumber = lineNumber;
	}

	public String getDriver()
	{
		return driver;
	}

	public void setDriver(String driver)
	{
		if (driver == null || driver.length() < 1)
		{
			throw new IllegalArgumentException(
					"Fahrer darf nicht leer sein oder weniger als einen Buchstaben haben. " + driver);
		}

		this.driver = driver;
	}

	public String getStart()
	{
		return start;
	}

	public void setStart(String start)
	{
		if (start == null || start.length() < 1)
		{
			throw new IllegalArgumentException(
					"Starthaltestelle darf nicht leer sein oder weniger als einen Buchstaben haben. " + start);
		}

		this.start = start;
	}

	public String getEnd()
	{
		return end;
	}

	public void setEnd(String end)
	{
		if (end == null || end.length() < 1)
		{
			throw new IllegalArgumentException(
					"Endhaltestelle darf nicht leer sein oder weniger als einen Buchstaben haben. " + end);
		}

		this.end = end;
	}

	@Override
	public String toString()
	{
		return String.format("%s: Linie %d von %s bis %s. Es fährt %s.", this.getClass().getSimpleName(), lineNumber, start, end, driver);
	}

	public boolean equals(TransportVehicle t)
	{
		return this.lineNumber == t.lineNumber;
	}
}
