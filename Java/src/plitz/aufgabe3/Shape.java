package plitz.aufgabe3;

public abstract class Shape
{
	protected double x;
	protected double y;

	public Shape(double x, double y)
	{
		this.x = x;
		this.y = y;
	}

	public static double getTotalArea(Shape... shapes)
	{
		double sum = 0;
		
		for (Shape s : shapes)
		{
			sum += s.getArea();
		}

		return sum;
	}
	
	public abstract double getArea();

	public abstract double getCircumference();

	@Override
	public String toString()
	{
		return String.format("%s: %f x %f - Area: %f - Circumference: %f", getClass().getSimpleName(), x, y, getArea(), getCircumference());
	}
	
}

class Circle extends Shape
{
	public Circle(double x)
	{
		super(x, x);
	}

	@Override
	public double getArea()
	{
		return Math.PI * x * x;
	}

	@Override
	public double getCircumference()
	{
		return 2 * Math.PI * x;
	}

	@Override
	public String toString()
	{
		return String.format("%s: %f - Area: %f - Circumference: %f", getClass().getSimpleName(), x, getArea(), getCircumference());
	}
	
}

class Rectangle extends Shape
{
	public Rectangle(double x, double y)
	{
		super(x, y);
	}

	@Override
	public double getArea()
	{
		return x * y;
	}

	@Override
	public double getCircumference()
	{
		return (x + y) * 2;
	}
}