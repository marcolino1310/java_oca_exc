package plitz.aufgabe3;

public class Bus extends TransportVehicle
{
	private int seats;
	
	public Bus(int lineNumber, String driver, String start, String end, int seats)
	{
		super(lineNumber, driver, start, end);
		setSeats(seats);
	}

	public int getSeats()
	{
		return seats;
	}

	public void setSeats(int seats)
	{
		if (seats < 1)
		{
			throw new IllegalArgumentException(
					"Es muss mindestens ein Sitz vorhanden sein. " + seats);
		}
		
		this.seats = seats;
	}

	@Override
	public String toString()
	{
		return super.toString() + String.format(" Der Bus hat %d Sitze.", seats);
	}
}
