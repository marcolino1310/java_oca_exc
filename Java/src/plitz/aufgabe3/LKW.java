package plitz.aufgabe3;

public class LKW extends Fahrzeug
{
	private double zuladung;

	public LKW()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	public LKW(String hersteller, String modell, int ps, double zuladung)
	{
		super(hersteller, modell, ps);
		setZuladung(zuladung);
	}

	public double getZuladung()
	{
		return zuladung;
	}

	public void setZuladung(double zuladung)
	{
		if (zuladung < 7.5 || zuladung > 40)
		{
			throw new IllegalArgumentException(
					"Zuladung darf nicht kleiner als 7,5 oder größer als 40 sein: " + zuladung);
		}
		this.zuladung = zuladung;
	}

	public String toString()
	{
		return super.toString() + " Zuladung: " + String.format("%.2f", zuladung) + ";";
	}
}
