package plitz.aufgabe3;

public class PersonenFahrzeug extends Fahrzeug
{
	private double speed;

	public PersonenFahrzeug()
	{
	}

	public PersonenFahrzeug(String hersteller, String modell, double ps, double speed)
	{
		super(hersteller, modell, ps);
		setSpeed(speed);
	}

	public double getSpeed()
	{
		return speed;
	}

	public void setSpeed(double speed)
	{
		if (speed < 30 || speed > 400)
		{
			throw new IllegalArgumentException(
					"Geschwindigkeit darf nicht kleiner als 30 oder größer als 400 sein: " + speed);
		}
		this.speed = speed;
	}

	public String toString()
	{
		return super.toString() + " Geschwindigkeit: " + String.format("%.2f", speed) + ";";
	}
}
