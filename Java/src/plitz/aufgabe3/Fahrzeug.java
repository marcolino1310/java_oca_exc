package plitz.aufgabe3;

public class Fahrzeug
{
	private String hersteller;
	private String modell;
	private double ps;

	public String getHersteller()
	{
		return hersteller;
	}

	public Fahrzeug()
	{

	}

	public Fahrzeug(String hersteller, String modell, double ps)
	{
		setHersteller(hersteller);
		setModell(modell);
		setPs(ps);
	}

	public void setHersteller(String hersteller) throws IllegalArgumentException
	{
		if (hersteller == null || hersteller.length() < 2)
		{
			throw new IllegalArgumentException(
					"Herstelller darf nicht leer sein oder weniger als 2 Zeichen haben: " + hersteller);
		}
		this.hersteller = hersteller;
	}

	public String getModell()
	{
		return modell;
	}

	public void setModell(String modell)
	{
		if (modell == null || modell.length() < 1)
		{
			throw new IllegalArgumentException(
					"Modell darf nicht leer sein oder weniger als 1 Zeichen haben: " + modell);
		}
		this.modell = modell;
	}

	public double getPs()
	{
		return ps;
	}

	public void setPs(double ps)
	{
		if (ps < 1 || ps > 500)
		{
			throw new IllegalArgumentException(
					"PS darf kleiner als 1 oder größer als 500 sein: " + ps);
		}
		this.ps = ps;
	}

	public String toString()
	{
		return "Hersteller: " + hersteller + "; Modell: " + modell + "; PS: " + ps + ";";
	}
}
