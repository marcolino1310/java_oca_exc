package plitz.aufgabe3;

public class Auto extends PersonenFahrzeug
{
	private int tueren;

	public Auto()
	{
	}

	public Auto(String hersteller, String modell, double ps, double speed, int tueren)
	{
		super(hersteller, modell, ps, speed);
		setTueren(tueren);
	}

	public int getTueren()
	{
		return tueren;
	}

	public void setTueren(int tueren)
	{
		if (tueren < 3 || tueren > 7)
		{
			throw new IllegalArgumentException("Tueren darf nicht kleiner als 2 oder größer als 7 sein: " + tueren);
		}
		this.tueren = tueren;
	}

	public String toString()
	{
		return super.toString() + " Türen: " + tueren + ";";
	}
}
