package plitz.aufgabe3;

public class Test
{
	public static void main(String[] args)
	{
		Hilfstyp[] testArray = new Hilfstyp[5];

		for (int i = 0; i < testArray.length; i++)
		{
			testArray[i].name = "" + i;
			testArray[i].umsatz = i * 50;
		}

		for (Hilfstyp i : testArray)
		{
			System.out.println(i.name + " " + i.umsatz);
		}
	}
}

class Hilfstyp
{
	String name;
	int umsatz;

	public Hilfstyp()
	{
	}

	public Hilfstyp(String name, int umsatz)
	{
		this.name = name;
		this.umsatz = umsatz;
	}
}