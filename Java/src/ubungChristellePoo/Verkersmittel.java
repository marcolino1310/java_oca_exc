package ubungChristellePoo;


public abstract class  Verkersmittel {
	private int linienNum;
	private String fahrerName;
	private String startPunkt;
	private String endPunkt;

	public Verkersmittel() {

	}

	public Verkersmittel(int lNum, String fName, String startP, String endP) {
		setLinienNum(lNum);
		setFahrerName(fName);
		setStartPunkt(startP);
		setEndPunkt(endP);
	}

	public void setLinienNum(int linienNum) {
		if (linienNum < 0)
			throw new IllegalArgumentException("Die Liniennummer darf nicht negativ sein");
		this.linienNum = linienNum;
	}

	public void setFahrerName(String fahrerName) {
		if (fahrerName.length() < 3)
			throw new IllegalArgumentException("FahreName muss mindesten 3 Zeichnen haben");
		this.fahrerName = fahrerName;
	}

	public void setStartPunkt(String startPunkt) {
		if (startPunkt.length() < 3)
			throw new IllegalArgumentException("Starthaltestelle muss mindesten 3 Zeichnen haben");
		this.startPunkt = startPunkt;
	}

	public void setEndPunkt(String endPunkt) {
		if (endPunkt.length() < 3)
			throw new IllegalArgumentException("Starthaltestelle muss mindesten 3 Zeichnen haben");
		this.endPunkt = endPunkt;
	}

	public int getLinienNum() {
		return linienNum;
	}

	public String getFahrerName() {
		return fahrerName;
	}

	public String getStartPunkt() {
		return startPunkt;
	}

	public String getEndPunkt() {
		return endPunkt;
	}

	public String toString() {

		return ("FahrerName: " + this.getFahrerName() + ", LinieNummer: " + this.getLinienNum() + ", von "
				+ this.getStartPunkt() + " bis " + this.getEndPunkt());

	}

	public boolean equals(Verkersmittel v) {
		if (this.getLinienNum() == v.getLinienNum()) 
			{return true;}
		else
		return false;
	}

}

/*
 * Modellieren Sie den �PNV. Ein �ffentliches Verkehrsmittel besitzt eine
 * Liniennummer, einen Fahrer, sowie eine Start- und eine Zielhaltestelle, ein
 * Bus besitzt zus�tzlich die Anzahl der Sitzpl�tze, eine S-Bahn hat �
 * zus�tzlich zu seinen Eigenschaften als Verkehrsmittel - den Namen des
 * Schaffners, die Anzahl der Wagen und die Anzahl der Sitzpl�tze pro Wagen. Die
 * Namen der Fahrer, Schaffner und Haltestellen werden als Zeichenketten
 * angegeben. Die Liniennumer, die Anzahl der Wagen und die Sitzpl�tze werden
 * als ganze Zahlen gespeichert. Die Verkehrsmittel sollen eine vern�nftige
 * Textdarstellung bekommen.
 * 
 * 
 * Programmieren Sie die Klassen Verkehrsmittel, Bus und S-Bahn, wobei Sie vom
 * Konzept der Vererbung Gebrauch machen sollen.
 * 
 * Erstellen Sie f�r jede Klasse den Standardkonstruktor sowie den Konstruktor,
 * der alle Attribute initialisiert.
 * 
 * Definieren Sie f�r die set-Methoden sinnvolle G�ltigkeits�berpr�fungen.
 * 
 * �berschreiben Sie die toString-Methode f�r eine formatierte Ausgabe aller
 * Attributwerte.
 * 
 * Definieren Sie die equals-Methode, so dass zwei Verkehrsmittel gleich sind
 * bei gleicher Liniennummer.
 */
