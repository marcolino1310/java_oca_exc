package ubungChristellePoo;

public class Bus extends Verkersmittel {
	private int sitzplatz;
	public Bus() {
		// TODO Auto-generated constructor stub
	}
	public Bus(int lNum, String fName, String startP, String endP,int sitzP) {
		super(lNum,fName,startP,endP);
		setSitzplatz(sitzP);
		
	}
	public void setSitzplatz(int sitzplatz) {
		if (sitzplatz <10 ||sitzplatz >90 )
			throw new IllegalArgumentException("Ein Bus muss mindesten 10  und Maximal 90 Sitzplštze haben");
		this.sitzplatz = sitzplatz;
	}
	public int getSitzplatz() {
		return sitzplatz;
	}

	public String toString() {

		return (super.toString() + ", Anzahl der Sitzplštze in Bus: " + this.getSitzplatz());

	}

}
