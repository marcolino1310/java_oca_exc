package ubungChristellePoo;

public abstract class Person {
	private String name;
	public Person() {
		// TODO Auto-generated constructor stub
	}
	public Person(String n) {
		 setName(n);
	}
	
  public void setName(String name) {
	  if(name==null || name.length()<3)
		  throw new IllegalArgumentException("Die Name muss mindesten 3 Wort haben");
	this.name = name;
}
  public String getName() {
	return name;
}
  public abstract int berechneGehalt();
  
  public static void display(Person p1,Person...p)
	
  {  long summe=p1.berechneGehalt();
  System.out.println("Name:"+p1.getName()+", Gehalt:"+ p1.berechneGehalt());
	 for(Person tab:p)
  {
	 summe+=tab.berechneGehalt();
	  System.out.println("Name:"+tab.getName()+", Gehalt:"+ tab.berechneGehalt());
  }
	 System.out.println("Summe aller Geh�lter: "+summe);
  }
  }
	  









/* 
Bilden Sie in Java eine Klassenhierarchie zur Gehaltsberechnung ab:  
 
Leiten Sie von einer abstrakten Basisklasse Person die zwei konkreten Unterklassen Angestellter und LeitenderAngestellter ab.
Alle Personen sollen �ber einen Namen verf�gen. Definieren Sie in Person eine abstrakte Methode berechneGehalt(), 
die in den konkreten Unterklassen jeweils unterschiedlich implementiert wird: 
das Gehalt f�r Angestellte betr�gt 2.000 Euro und erh�ht sich um 50 Euro je �berstunde(Attribut der Klasse Angestellter). 
Das Gehalt f�r leitende Angestellte betr�gt 3.000 Euro plus Bonus (Attribut der Klasse LeitenderAngestellter).
Leitende Angestellte machen keine �berstunden!
Schreiben Sie weiterhin eine Methode, die eine beliebige Anzahl von Person-Objekten als Parameter erh�lt, f�r jede Person jeweils Name und Gehalt sowie die Gesamtsumme der Geh�lter aller Personen ausgibt.  
 
Setzen Sie die Anforderungen in Java um und implementieren Sie die Methoden. 
 
�berpr�fen Sie Ihre Implementierung mit einem Beispielprogramm mit main-Methode. 
 */