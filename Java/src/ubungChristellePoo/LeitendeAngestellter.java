package ubungChristellePoo;

public class LeitendeAngestellter extends Person {
	private static final int GEHALT=3000;
	private int bonus=0;
	public LeitendeAngestellter() {
		// TODO Auto-generated constructor stub
	}
	public LeitendeAngestellter(String name, int b) {
		super(name);
		setBonus(b);
	}
	public void setBonus(int bonus) {
		if(bonus<0)
			throw new IllegalArgumentException("Bonus darf nicht negativ sein");
		this.bonus = bonus;
	}
	public int getBonus() {
		return bonus;
	}
	@Override
	public int  berechneGehalt() {
		
		return GEHALT+this.bonus;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
