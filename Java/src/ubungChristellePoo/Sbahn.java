package ubungChristellePoo;

public class Sbahn extends Verkersmittel {
	private String schaffnerName;
	private int wagenAnzahl;
	private int wagenSitzA;

	public Sbahn() {
	}

	public Sbahn(int lNum, String fName, String startP, String endP, String schaffnerN, int wagenA, int wagenSA) {
		super(lNum, fName, startP, endP);
		setSchaffnerName(schaffnerN);
		setWagenAnzahl(wagenA);
		setWagenSitzA(wagenSA);

	}

	public void setSchaffnerName(String schaffnerName) {
		if (schaffnerName.length() < 3)
			throw new IllegalArgumentException("FahreName muss mindesten 3 Zeichnen haben");
		this.schaffnerName = schaffnerName;
	}

	public void setWagenAnzahl(int wagenAnzahl) {
		if (wagenAnzahl < 1 || wagenAnzahl > 10)
			throw new IllegalArgumentException("Ein S-Bahn muss mindesten 1  und Maximal 10 Wagen haben");
		this.wagenAnzahl = wagenAnzahl;
	}

	public void setWagenSitzA(int wagenSitzA) {
		if (wagenSitzA <= 0 || wagenSitzA > 500)
			throw new IllegalArgumentException(
					"Ein S-BahnWagen muss Maximal 90 Sitzplštze haben und dieser Zahl kann nicht negativ oder gleich null sein");
		this.wagenSitzA = wagenSitzA;
	}

	public String getSchaffnerName() {
		return schaffnerName;
	}

	public int getWagenAnzahl() {
		return wagenAnzahl;
	}

	public int getWagenSitzA() {
		return wagenSitzA;
	}

	public String toString() {

		return (super.toString() + ", Anzahl der Wagen " + this.getWagenAnzahl() + ", Anzahl der Sitzplštze pro Wagen: "
				+ this.getWagenSitzA() + ", Name des Schaffners :" + this.getSchaffnerName());

	}

}
