package ubungChristellePoo;

public class Angestellter extends Person {
	private static final int GEHALT=2000;
	private int ueberstd=0;
	public Angestellter() {
		
	}
	public Angestellter(String name,int u) {
		super(name);
		setUeberstd(u);
		
		
	}
	public void setUeberstd(int ueberstd) {
		if(ueberstd<0)
			throw new IllegalArgumentException("�berstundezahl darf nicht negativ sein");
		this.ueberstd = ueberstd;
	}
	public int getUeberstd() {
		return ueberstd;
	}
	@Override
	public int berechneGehalt() {
		return GEHALT+(this.ueberstd*50);
	}

}























/* 
Bilden Sie in Java eine Klassenhierarchie zur Gehaltsberechnung ab:  
 
Leiten Sie von einer abstrakten Basisklasse Person die zwei konkreten Unterklassen Angestellter und LeitenderAngestellter ab.
Alle Personen sollen �ber einen Namen verf�gen. Definieren Sie in Person eine abstrakte Methode berechneGehalt(), 
die in den konkreten Unterklassen jeweils unterschiedlich implementiert wird: 
das Gehalt f�r Angestellte betr�gt 2.000 Euro und erh�ht sich um 50 Euro je �berstunde(Attribut der Klasse Angestellter). 
Das Gehalt f�r leitende Angestellte betr�gt 3.000 Euro plus Bonus (Attribut der Klasse LeitenderAngestellter).
Leitende Angestellte machen keine �berstunden!
Schreiben Sie weiterhin eine Methode, die eine beliebige Anzahl von Person-Objekten als Parameter erh�lt, f�r jede Person jeweils Name und Gehalt sowie die Gesamtsumme der Geh�lter aller Personen ausgibt.  
 
Setzen Sie die Anforderungen in Java um und implementieren Sie die Methoden. 
 
�berpr�fen Sie Ihre Implementierung mit einem Beispielprogramm mit main-Methode. 
 */